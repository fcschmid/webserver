# Webserver and Wordpress <a name="overview"></a>

## Getting started 

This tutorial shows how a typical webserver and CMS can be deployed on a container orchestration using Docker and Docker Compose. 

If you need to install a container orchestration first, you can follow this Tutorial: [https://gitlab.com/fcschmid/container-orc-setup](https://gitlab.com/fcschmid/container-orc-setup)


### Annotation:
Hostname: webapp
USER: pi

## Step 1:
We create a new folder for containers under the user:

    sudo mkdir containers

We change to the directory

    cd containers

We create a directory with subfolder for the IoTStack of the dataenlogger

    sudo mkdir -p ./wordpress/{database,app}
    
We change to the directory

    cd wordpress

We assign new permissions to each created subfolder:

    sudo chown 1001:1001 database/
    sudo chown 1001:1001 app/

## Step 2: 
We now create the Docker Compose file. It declares each service and the network configuration for the Docker network. 
This will start the containers and connect them to each other. It also declares the deployment to the outside world to use them as services.
To ensure that the settings are persistent, volumes are declared accordingly.
The ports part specifies which ports are mapped from the container.

We create the file:

    sudo nano docker-compose.yml

First we declare the version of docker-compose.yml

    version: "3"

We create a section for the services:

    services:

Under this, the wordpress app and the appropriate database are. In yml files it is important to pay attention to the spaces and declaration characters:

    version: "3"

    services:
      wordpress:
        image: wordpress
        restart: unless-stopped
        container_name: wordpress
        environment:
          WORDPRESS_DB_HOST: wordpress-db
          WORDPRESS_DB_USER: exampleuser    ## Hier Benutzer eingeben ##
          WORDPRESS_DB_PASSWORD: examplepass ## Hier Passwort eingeben ##
          WORDPRESS_DB_NAME: wordpress
        ports:
          - "192.168.178.65:80:80"
        volumes:
          - ./app:/var/www/html
      wordpress-db:
        image: mariadb:10.6.4-focal
        container_name: wordpress-db
        restart: unless-stopped
        environment:
          MYSQL_DATABASE: wordpress
          MYSQL_USER: exampleuser     ## Hier selben Benutzer eingeben ##
          MYSQL_PASSWORD: examplepass ## Hier selbes Passwort eingeben ##
          MYSQL_RANDOM_ROOT_PASSWORD: '1'
        volumes:
          - ./database:/var/lib/mysql




## Step 3: 
Now we are ready to start the stack.
We start the creation of the containers with the following command:

    sudo docker-compose up -d 

Now we test the whole thing: 

    docker ps

Then we navigate in a browser to

    webapp:1880

## General Administration: 
To terminate the stack we enter the following in the kmando line:

    docker compose down

To also delete the persistent volumes when we no longer need them, we enter the following in the kmando line:

    docker volume prune 

To delete the images, we enter the following in the Kmando line:

    docker image prune 

To detach the network settings, we enter the following in the kmando line:

    docker network prune



# Navigation

[Overview](0_README.md)


